aspell-ml (0.04-1-10) unstable; urgency=low

  [ Debian Janitor ]
  * Use versioned copyright format URI.
  * Fix day-of-week for changelog entry 0.01-1-1.
  * Remove constraints unnecessary since buster:
    + Build-Depends-Indep: Drop versioned constraint on aspell.
    + aspell-ml: Drop versioned constraint on aspell and dictionaries-common in
      Depends.
  * Remove constraints unnecessary since buster:
    + Build-Depends-Indep: Drop versioned constraint on
      dictionaries-common-dev.

  [ Kartik Mistry ]
  * debian/control:
    + Updated Standards-Version to 4.6.0
    + Bumped dh to 13.
  * Updated debian/copyright.
  * Updated debian/watch.

 -- Kartik Mistry <kartik@debian.org>  Tue, 14 Jun 2022 19:00:44 +0530

aspell-ml (0.04-1-9) unstable; urgency=low

  * Added debian/gitlab-ci.yml.
  * debian/control:
    + Updated Standards-Version to 4.5.0
    + Switched to debhelper-compat.
    + Added 'Rules-Requires-Root' field.
    + Update Section to localization.
  * debian/rules:
    + Use --with aspell-simple.

 -- Kartik Mistry <kartik@debian.org>  Wed, 08 Apr 2020 23:04:20 +0530

aspell-ml (0.04-1-8) unstable; urgency=low

  * debian/control:
    + Updated Maintainer email.
    + Updated Vcs-* fields.
    + Updated Standards-Version to 4.2.1
  * Bumped dh to 11.
  * Updated debian/copyright.
  * Use https in debian/watch.

 -- Kartik Mistry <kartik@debian.org>  Sat, 08 Dec 2018 12:10:18 +0530

aspell-ml (0.04-1-7) unstable; urgency=medium

  [ Kartik Mistry ]
  * Rebuild.
  * debian/control:
    + Bumped Standards-Version to 3.9.7 (no changes).
    + Fixed Vcs-* URLs.
    + Bumped debhelper dependency to 9.
  * debian/rules:
    + Make aspell-ml reproducible (Closes: #769016)

 -- Kartik Mistry <kartik@debian.org>  Sun, 27 Mar 2016 17:01:34 +0530

aspell-ml (0.04-1-6) unstable; urgency=low

  [ Kartik Mistry ]
  * debian/control:
    + Bumped Standards-Version to 3.9.5
    + Fixed VCS-* URLs.
  * debian/copyright:
    + Updated Debian copyrights.

 -- Kartik Mistry <kartik@debian.org>  Fri, 01 Aug 2014 10:04:40 +0000

aspell-ml (0.04-1-5) unstable; urgency=low

  [Kartik Mistry]
  * debian/copyright:
    + Updated Format URL according to DEP-5 specification.
    + Better License paragrah arrangements.

  [Vasudev Kamath]
  * debian/control:
    + Changed Vcs-* fields to point to new git repository.
  * debian/rules:
    + Removed var/lib/aspell from md5sums calculation in rules file.
      (Closes: #638731)

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Thu, 03 Nov 2011 17:08:54 +0530

aspell-ml (0.04-1-4) unstable; urgency=low

  [Vasudev Kamath]
  * debian/rules
    + Changed to dh7 standard
  * debian/control
    + Changed the HomePage URL
    + Bumped Standards-Version to 3.9.2

  [Kartik Mistry]
  * debian/copyright:
    + Used DEP-5 format

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 04 Jun 2011 18:05:48 +0530

aspell-ml (0.04-1-3) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.8.4 (no changes needed)
    + Updated extented package description
    + Wrapped up Dependencies and Uploaders fields
  * Updated package to new source format 3.0 (quilt)

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Mon, 01 Mar 2010 22:39:09 +0530

aspell-ml (0.04-1-2) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.8.1
    + Updated my maintainer email address
    + [Lintian] Added ${misc:Depends} for debhelper dependency

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Thu, 18 Jun 2009 17:24:46 +0530

aspell-ml (0.04-1-1) unstable; urgency=low

  [Kartik Mistry]
  * New upstream release
  * debian/control:
    + Updated package descriptions
    + Added homepage entry
    + Updated Standards-Version to 3.7.3
    + Added VCS-* fields
  * debian/copyright:
    + Updated download location
  * debian/watch:
    + Updated for new download location

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 26 Jan 2008 14:42:41 +0530

aspell-ml (0.03-1-1) unstable; urgency=low

  [Kartik Mistry]
  * New upstream release
  * Added debian/watch file
  * debian/copyright: updated license, updated download location

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Fri, 12 Oct 2007 16:36:14 +0530

aspell-ml (0.01-1-1) unstable; urgency=low

  [Kartik Mistry]
  * Initial release (Closes: #440295)

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 31 Mar 2007 16:00:37 +0530
